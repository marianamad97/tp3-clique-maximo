import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class Grafo {
	private boolean[][] matrizAdyacencia;
	private ArrayList<Vertice> vertices;
	private Set<Integer>indicesCliquePesoMax=new HashSet<Integer>();
	double pesoCliquePesoMax=0;

	public Grafo() {
		this.vertices= new ArrayList<>();
		//this.matrizAdyacencia = new boolean[cantVerices][cantVerices];
	}

	public void agregarVertice(double peso) {
		Vertice v = new Vertice(peso);
		System.out.println(v.getIndice() + "vertice agregado " + "peso: " + v.getPeso());
		this.vertices.add(v);
	}

	public void agregarArco(Integer indiceV1, Integer indiceV2) {
		if(existeArco(indiceV1, indiceV2)) {
			throw new IllegalArgumentException("Ya existe el arco entre el vertice " + indiceV1 + " y el vertice "+ indiceV2);
		}
		else
			this.matrizAdyacencia[indiceV1][indiceV2] = true;
			this.matrizAdyacencia[indiceV2][indiceV1] = true;
	}
	
	public void setMatrizAdyacencia(int cantVertices) {
		this.matrizAdyacencia=new boolean[cantVertices][cantVertices];
		
	}
	
	public boolean [][] getMatrizAdyacencia() {
		return matrizAdyacencia;
	}
	
	
	
	public ArrayList<Vertice> getVertices() {
		return this.vertices;
	}
	
	public Set<Integer> getCliquePesoMax(){
		return this.indicesCliquePesoMax;
	}

	public boolean existeArco(int i, int j) {
		verificarVertice(i);
		verificarVertice(j);
		verificarDistintos(i, j);

		return matrizAdyacencia[i][j];
	}

	// Cantidad de vertices
	public int tamano() {
		return matrizAdyacencia.length;
	}
	
	// Vecinos de un vertice
	public Set<Integer> vecinos(int i)
	{
		verificarVertice(i);
		
		Set<Integer> ret = new HashSet<Integer>();
		for(int j = 0; j < this.tamano(); ++j) if( i != j )
		{
			if( this.existeArco(i,j) )
				ret.add(j);
		}
		
		return ret;
	}
	
	public Integer grado(Integer i)
	{
		verificarVertice(i);
		return vecinos(i).size();
	}
	
	//Devuelve una lista con los vertices ordenados por peso descendente.
	public ArrayList<Vertice> verticesOrdenadosPorPeso(){
		
		ArrayList<Vertice> copiaOrdenada = this.getVertices();
		ArrayList<Vertice> ret = new ArrayList<>(copiaOrdenada);
		Collections.sort(ret);
		
		System.out.println("Prueba sin ordenar");
		for(int i=0; i<ret.size(); i++) {
			System.out.println(ret.get(i).getIndice() + " peso: " + ret.get(i).getPeso());
			
		}
		
		Collections.sort(ret);
		System.out.println("Prueba metodo vertices ordenados x peso. tam" + ret.size());
		for(int i=0; i<ret.size(); i++) {
			System.out.println(ret.get(i).getIndice()+ " peso: " + ret.get(i).getPeso());
		}
	
		return ret;
		
	}
	
	
	public Set<Integer> vecinosEntreTodos(Vertice v)
	{
		Set<Integer> vecinosEntreTodos=new HashSet<Integer>();
		Set<Integer> vecinosDelVertice=this.vecinos(v.getIndice());
		ArrayList<Integer> vecinosDeV = new ArrayList<>(vecinosDelVertice); 
		//convierto el set en una lista para poder acceder a los elementos de la matriz de adyacencia
		
		if(this.grado(v.getIndice())>1) {
			
			for(int i=0; i<vecinosDeV.size(); i++) {
				
				for(int j=i+1; j<vecinosDeV.size(); j++) {
					
					System.out.println("vecinosEntreTodos  "+  v.getIndice() + "  "  + vecinosDeV.get(i) + "  " + vecinosDeV.get(j)  );
					if(this.existeArco(vecinosDeV.get(i), vecinosDeV.get(j)))  {
						System.out.println("existe arco");
						vecinosEntreTodos.add(v.getIndice());
						vecinosEntreTodos.add(vecinosDeV.get(i));
						vecinosEntreTodos.add(vecinosDeV.get(j));
					}
				}
			}
		}
		 return vecinosEntreTodos;
	}
	
	
	
	//cliquePesoMax
	public Set<Integer> indicesVerticesCliquePesoMax(){
		
		ArrayList<Vertice> conjVertices=verticesOrdenadosPorPeso();
		
		for(int i=0; i<conjVertices.size(); i++) {
			Set<Integer> a=this.vecinosEntreTodos(conjVertices.get(i));
			System.out.println("a.size " + a.size());
			if(a.size()>2){
				this.indicesCliquePesoMax=a;
				return indicesCliquePesoMax;
			}
		}
		return indicesCliquePesoMax;
	}
	
	public double getPesoClique() {
		
		for(Integer i : this.getCliquePesoMax()) {
			this.pesoCliquePesoMax=this.pesoCliquePesoMax+this.getVertices().get(i).getPeso();
			System.out.println("-------------------------------------------");
			System.out.println("indice: " + this.getVertices().get(i).getIndice());
			System.out.println("peso: "+ this.getVertices().get(i).getPeso());
		}
		
		return pesoCliquePesoMax;
	}
	
	
	
	// Verifica que sea un vertice valido
	private void verificarVertice(int i) {
		if (i < 0)
			throw new IllegalArgumentException("El vertice no puede ser negativo: " + i);

		if (i >= matrizAdyacencia.length)
			throw new IllegalArgumentException("Los vertices deben estar entre 0 y |V|-1: " + i);
	}

	// Verifica que i y j sean distintos
	private void verificarDistintos(int i, int j) {
		if (i == j)
			throw new IllegalArgumentException("No se permiten loops: (" + i + ", " + j + ")");
	}

}
